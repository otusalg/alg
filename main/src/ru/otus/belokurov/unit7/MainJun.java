package ru.otus.belokurov.unit7;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainJun {

    public static void main(String[] args) {
        System.out.println("selection sort");
        printSort(MainJun::selectionSort, randomIntegerList(100));
        printSort(MainJun::selectionSort, randomIntegerList(1000));
        printSort(MainJun::selectionSort, randomIntegerList(10000));

        System.out.println("heap sort");
        printSort(MainJun::heapSort, randomIntegerList(100));
        printSort(MainJun::heapSort, randomIntegerList(1000));
        printSort(MainJun::heapSort, randomIntegerList(10000));

    }

    public static List<Integer> selectionSort(List<Integer> list) {
        for (int i = list.size() - 1; i > 0; i--) {
            int max = i;
            for (int j = i - 1; j >= 0; j--) {
                if (list.get(max) < list.get(j)) {
                    max = j;
                }
            }
            int maxValue = list.get(max);
            list.set(max, list.get(i));
            list.set(i, maxValue);
        }
        return list;
    }

    public static List<Integer> heapSort(List<Integer> list) {
        for (int i = list.size() / 2 - 1; i >= 0; i--) {
            heapify(i, list.size(), list);
        }
        for (int i = list.size() - 1; i >= 0; i--) {
            swap(0, i, list);
            heapify(0, i, list);
        }
        return list;
    }

    private static void heapify(int i, int size, List<Integer> list) {
        int root = i;
        int left = 2 * i + 1;
        int right = left + 1;
        if (left < size && list.get(left) > list.get(root)) {
            root = left;
        }
        if (right < size && list.get(right) > list.get(root)) {
            root = right;
        }
        if (root == i) {
            return;
        }
        swap(i, root, list);
        heapify(root, size, list);
    }

    private static void swap(int i, int j, List<Integer> list) {
        int temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }


    public static void printSort(Function<List<Integer>, List<Integer>> sortFunction, List<Integer> list) {
        long start = System.currentTimeMillis();
        sortFunction.apply(list);
        long time = System.currentTimeMillis() - start;
        System.out.println("Массив размером : " + list.size() + " отсортирован за " + time);

    }

    public static List<Integer> randomIntegerList(int n) {
        return Stream.iterate(0, i -> (int) (Math.random() * 100)).limit(n).collect(Collectors.toList());
    }

}
