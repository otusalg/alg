package ru.otus.belokurov.unit6;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainJun {

    public static void main(String[] args) {
        System.out.println("Bubble sort");
        printSort(MainJun::bubbleSort, randomIntegerList(100));
        printSort(MainJun::bubbleSort, randomIntegerList(1000));
        printSort(MainJun::bubbleSort, randomIntegerList(10000));

        System.out.println("Insert sort");
        printSort(MainJun::insertSort, randomIntegerList(100));
        printSort(MainJun::insertSort, randomIntegerList(1000));
        printSort(MainJun::insertSort, randomIntegerList(10000));

        System.out.println("Shell sort");
        printSort(MainJun::shellSort, randomIntegerList(100));
        printSort(MainJun::shellSort, randomIntegerList(1000));
        printSort(MainJun::shellSort, randomIntegerList(10000));

    }

    public static List<Integer> bubbleSort(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size() - 1; j++) {
                if (list.get(j) > list.get(j + 1)) {
                    int temp = list.get(j);
                    list.set(j , list.get(j + 1));
                    list.set(j + 1, temp);
                }
            }
        }
        return list;
    }

    public static List<Integer> insertSort(List<Integer> list) {
        for (int i = 1; i < list.size(); i++) {
            int j = i;
            while (j != 0 && list.get(j) < list.get(j - 1)) {
                int temp = list.get(j);
                list.set(j, list.get(j - 1));
                list.set(j - 1, temp);
                j--;
            }
        }
        return list;
    }

    public static List<Integer> shellSort(List<Integer> list) {
        int n = list.size();
        for (int gap = n / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < n; i++) {
                for (int j = i; j >= gap ; j -= gap) {
                    if (list.get(j - gap) > list.get(j)){
                        int temp = list.get(j - gap);
                        list.set(j - gap, list.get(j));
                        list.set(j, temp);
                    }
                }
            }
        }
        return list;
    }

    public static void printSort(Function<List<Integer>, List<Integer>> sortFunction, List<Integer> list) {
        long start = System.currentTimeMillis();
        sortFunction.apply(list);
        long time = System.currentTimeMillis() - start;
        System.out.println("Массив размером : " + list.size() + " отсортирован за " + time);

    }

    public static List<Integer> randomIntegerList(int n) {
        return Stream.iterate(0, i -> (int) (Math.random() * 100)).limit(n).collect(Collectors.toList());
    }
}
