package ru.otus.belokurov.unit8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainJun {

    public static void main(String[] args) {
        System.out.println("quick sort");
        printSort(MainJun::quickSort, randomIntegerList(100));
        printSort(MainJun::quickSort, randomIntegerList(1000));
        printSort(MainJun::quickSort, randomIntegerList(10000));

        System.out.println("merge sort");
        printSort(MainJun::mergeSort, randomIntegerList(100));
        printSort(MainJun::mergeSort, randomIntegerList(1000));
        printSort(MainJun::mergeSort, randomIntegerList(10000));

    }

    public static List<Integer> quickSort(List<Integer> list) {
        return quick(list, 0, list.size() - 1);
    }

    public static List<Integer> mergeSort(List<Integer> list) {
        return mergeSort(list, 0, list.size() - 1);
    }

    private static List<Integer> mergeSort(List<Integer> list, int l, int r) {
        if (l >= r) {
            return list;
        }
        int m = (l + r) / 2;
        mergeSort(list, l, m);
        mergeSort(list, m + 1, r);
        merge(l, m, r, list);
        return list;
    }

    private static void merge(int l, int m, int r, List<Integer> list) {
        int [] T = new int [r - l + 1];
        int a = l;
        int b = m + 1;
        int c = 0;
        while (a <= m && b <= r) {
            if (list.get(a) <= list.get(b)) {
                T[c++] = list.get(a++);
            } else {
                T[c++] = list.get(b++);
            }
        }
        while (a <= m) {
            T[c++] = list.get(a++);
        }
        while (b <= r) {
            T[c++] = list.get(b++);
        }
        for (int i = l; i <= r; i++) {
            list.set(i, T[i - l]);
        }
    }

    public static List<Integer> quick(List<Integer> list, int l, int r) {
        if (l >= r) {
            return list;
        }
        int m = split(list, l, r);
        quick(list, l, m - 1);
        quick(list, m + 1, r);
        return list;
    }

    private static int split(List<Integer> list, int l, int r) {
        int p = list.get(r);
        int m = l - 1;
        for (int i = l; i <= r; i++) {
            if (list.get(i) <= p) {
                swap(++m, i, list);
            }
        }
        return m;
    }

    private static void swap(int i, int j, List<Integer> list) {
        int temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }


    public static void printSort(Function<List<Integer>, List<Integer>> sortFunction, List<Integer> list) {
        long start = System.currentTimeMillis();
        sortFunction.apply(list);
        long time = System.currentTimeMillis() - start;
        System.out.println("Массив размером : " + list.size() + " отсортирован за " + time);

    }

    public static List<Integer> randomIntegerList(int n) {
        return Stream.iterate(0, i -> (int) (Math.random() * 100)).limit(n).collect(Collectors.toList());
    }

}
