package ru.otus.belokurov.unit3;

public class MainMid {

    private static final int F1 = 1;
    private static final int F2 = 1;

    public static void main(String[] args) {
        System.out.printf("возведения в степень через двоичное разложение показателя степени O(LogN) : %d %n", powRecursive(2, 3));
        System.out.printf("алгоритм поиска чисел Фибоначчи по формуле золотого сечения : %d %n", fibWithGoldSect(8));
        fibWithGoldSect(5);
    }

    public static int powRecursive(int n, int k) {
        if (k == 1) {
            return n;
        }

        if (k % 2 == 0) {
            int result = powRecursive(n, k / 2);
            return result * result;
        } else {
            int result = powRecursive(n, k - 1);
            return result * n;
        }
    }

    public static int fibWithGoldSect(double n) {
        return (int) (Math.pow(1.618, n) / Math.sqrt(5) + 1/2);
    }


}