package ru.otus.belokurov.unit3;

import java.util.function.Consumer;

public class MainJun {

    private static final int F1 = 1;
    private static final int F2 = 1;

    public static void main(String[] args) {
        pow(2, 4);
        System.out.println("Фибаначи через рекурсию за O(2^N) : " + fibNumWithRecurs(8));
        System.out.println("Фибаначи через итерации за O(N) : " + fibNumWithIteration(8));
    }

    public static void pow(int n, int k) {
        int result = 1;
        for (int i = 0; i < k; i++) {
            result = result * n;
        }
        System.out.printf("Возведение числа %d в степень %d за O(N) : %d %n", n, k, result);
    }

    public static int fibNumWithRecurs(int n) {
        if (n == 2) {
            return F2;
        }
        if (n == 1) {
            return F1;
        }
        return fibNumWithRecurs(n - 1) + fibNumWithRecurs(n - 2);
    }

    public static int fibNumWithIteration(int n) {
        int f1 = F1;
        int f2 = F2;
        int res = 0;

        for (int i = 2; i < n; i++) {
            res = f1 + f2;
            f1 = f2;
            f2 = res;
        }

        return res;
    }

    private void t(String o) {
    }


}