package ru.otus.belokurov.unit5;

public class MainJun {
    public static void main(String[] args) {
        long a = 40;
        long k = 1L << a;
        System.out.println(king(k));
        System.out.println(horse(k));

    }

    private static long king(long n) {
        /**
         * Маска равна всем полям кроме левой диаганали в случае если король стоит на левой диагонале,
         * мы получим KLeft равно нулю через операцию И. Также для правой диагонали.
         */
        long KLeft = n & 0xfefefefefefefefeL;
        long KRight = n & 0x7f7f7f7f7f7f7f7fL;
        return  KRight << 9 |  n << 8  | KLeft << 7 |
                KRight << 1 |            KLeft >> 1 |
                KRight >> 7 |  n >> 8  | KLeft >> 9 ;

    }

    private static long horse(long n) {
        long HLongLeft = n & 0xfcfcfcfcfcfcfcfcL;
        long HShortLeft = n & 0xfefefefefefefefeL;
        long HLongRight = n & 0x3f3f3f3f3f3f3f3fL;
        long HShortRight = n & 0x7f7f7f7f7f7f7f7fL;
        return  HLongRight << 10  |  HShortRight << 17 | HShortLeft << 15 | HLongLeft << 6 |

                HLongRight >> 6  |  HShortRight >> 15 | HShortLeft >> 17 |  HLongLeft >> 10 ;

    }
}
