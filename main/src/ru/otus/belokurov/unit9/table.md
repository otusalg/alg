| Alg            | 100 | 1000 | 10000 |
|----------------|-----|------|-------|
| Bubble sort    | 2   | 34   | 301   |
| Insert sort    | 1   | 17   | 109   |
| Shell sort     | 1   | 12   | 177   |
| Selection sort | 1   | 11   | 146   |
| Heap sort      | 1   | 1    | 7     |
| quick sort     | 0   | 1    | 19    |
| merge sort     | 0   | 0    | 5     |
| bucket sort    | 0   | 1    | 3     |
| counting sort  | 0   | 0    | 3     |
| radix sort     | 0   | 1    | 11    |