package ru.otus.belokurov.unit9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainJun {

    public static void main(String[] args) {
        System.out.println("bucket sort");
        printSort(MainJun::bucketSort, randomIntegerList(100));
        printSort(MainJun::bucketSort, randomIntegerList(1000));
        printSort(MainJun::bucketSort, randomIntegerList(10000));

        System.out.println("counting sort");
        printSort(MainJun::countingSort, randomIntegerList(100));
        printSort(MainJun::countingSort, randomIntegerList(1000));
        printSort(MainJun::countingSort, randomIntegerList(10000));

        System.out.println("radix sort");
        printSort(MainJun::radixSort, randomIntegerList(100));
        printSort(MainJun::radixSort, randomIntegerList(1000));
        printSort(MainJun::radixSort, randomIntegerList(10000));


    }

    public static List<Integer> radixSort(List<Integer> list) {
        list = characterList(list, 1);
        list = characterList(list, 0);
        return list;
    }

    public static List<Integer> characterList(List<Integer> list, int pos) {
        List<Character> characterList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            characterList.add(list.get(i).toString().charAt(pos));
        }
        char maxVal = characterList.getFirst();
        for (char val : characterList) {
            if (val > maxVal) {
                maxVal = val;
            }
        }
        int[] counter = new int[10];
        for (int i = 0; i < characterList.size(); i++) {
            counter[Integer.parseInt(String.valueOf(characterList.get(i)))]++;
        }
        for (int i = 1; i < counter.length; i++) {
            counter[i] += counter[i - 1];
        }
        Integer[] result = new Integer[list.size()];
        for (int i = list.size() - 1; i >= 0; i--) {
            int val = Integer.parseInt(String.valueOf(characterList.get(i)));
            result[counter[val] - 1] = list.get(i);
            counter[val]--;
        }
        return Arrays.asList(result);
    }

    public static List<Integer> countingSort(List<Integer> list) {
        int maxVal = list.getFirst();
        for (int val : list) {
            if (val > maxVal) {
                maxVal = val;
            }
        }
        int[] counter = new int[++maxVal];
        for (int i = 0; i < list.size(); i++) {
            counter[list.get(i)]++;
        }
        for (int i = 1; i < counter.length; i++) {
            counter[i] += counter[i - 1];
        }
        Integer[] result = new Integer[list.size()];
        for (int i = list.size() - 1; i >= 0; i--) {
            result[counter[list.get(i)] - 1] = list.get(i);
            counter[list.get(i)]--;
        }
        return Arrays.asList(result);
    }


    public static List<Integer> bucketSort(List<Integer> list) {
        int maxVal = list.get(0);
        for (int val : list) {
            if (val > maxVal) {
                maxVal = val;
            }
        }
        LinkedList[] buckets = new LinkedList[list.size()];
        for (int val : list) {
            int nr = val * list.size() / ++maxVal;
            if (buckets[nr] == null) {
                buckets[nr] = new LinkedList(val);
            } else {
                LinkedList l = buckets[nr];
                while (l.next != null && l.next.value < val) {
                    l = l.next;
                }
                LinkedList newL = new LinkedList(val, l.next);
                l.next = newL;
            }
        }

        List<Integer> result = new ArrayList<>();
        for (LinkedList bucket : buckets) {
            LinkedList b = bucket;
            while (b != null) {
                result.add(b.value);
                b = b.next;
            }
        }
        return result;
    }

    public static void printSort(Function<List<Integer>, List<Integer>> sortFunction, List<Integer> list) {
        long start = System.currentTimeMillis();
        sortFunction.apply(list);
        long time = System.currentTimeMillis() - start;
        System.out.println("Массив размером : " + list.size() + " отсортирован за " + time);

    }

    public static List<Integer> randomIntegerList(int n) {
        return Stream.iterate(10, i -> (int) (Math.random() * 90) + 10).limit(n).collect(Collectors.toList());
    }


    private static class LinkedList {
        private int value;
        private LinkedList next;

        public LinkedList() {}

        public LinkedList(int value) {
            this.value = value;
        }

        public LinkedList(int value, LinkedList next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }

        public LinkedList getNext() {
            return next;
        }
    }
}
