package ru.otus.belokurov.unit2;

public class MainJun {
    public static void main(String[] args) {
        int count = 0;
        for (int a1 = 0; a1 < 10; a1++) {
            for (int a2 = 0; a2 < 10; a2++) {
                int sumA = a1 + a2;
                for (int b1 = 0; b1 < 10; b1++) {
                    for (int b2 = 0; b2 < 10; b2++) {
                        int sumB = b1 + b2;
                        int abs = Math.abs(sumA - sumB);
                        if (abs < 10){
                            count += 10 - abs;
                        }
                    }
                }
            }
        }
        System.out.println(count);
    }
}
