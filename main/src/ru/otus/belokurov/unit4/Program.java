package ru.otus.belokurov.unit4;


import ru.otus.belokurov.unit4.model.*;

import java.util.Date;
import java.util.Objects;

public class Program {

    public static void main(String[] args) {
        IArray singleArray = new SingleArray();
        IArray vectorArray = new VectorArray();
        IArray factorArray = new FactorArray();
        IArray matrixArray = new MatrixArray();
        testAddArray(singleArray, 10_000);
        testAddArray(vectorArray, 100_000);
        testAddArray(factorArray, 100_000);
        testAddArray(matrixArray, 100_000);

        testAddAndRemoveIndex(new SingleArray());
        testAddAndRemoveIndex(new VectorArray());
        testAddAndRemoveIndex(new FactorArray());
        testAddAndRemoveIndex(new MatrixArray());
    }

    private static void testAddArray(IArray data, int total) {
        long start = System.currentTimeMillis();

        for (int j = 0; j < total; j ++)
            data.add(new Date());

        System.out.println(data + " testAddArray: " +
                (System.currentTimeMillis() - start));
    }

    private static void testAddAndRemoveIndex(IArray data) {
        String str = "test";
        int index = 101;
        data.add(str, index);
        if (data.get(index).equals(str)) {
            System.out.println(data.getClass().getName() + " : Success add");
        }
        data.remove(index);
        if (Objects.isNull(data.get(index))) {
            System.out.println(data.getClass().getName() + " : Success remove");
        }
    }
}
