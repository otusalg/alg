package ru.otus.belokurov.unit4.model;

public class SingleArray<T> implements IArray<T> {

    private Object[] array;

    public SingleArray () {
        array = new Object[0];
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public void add(T item) {
        resize();
        array[size() - 1] = item;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        return (T) array[index];
    }

    @Override
    public void add(T item, int index) {
        if (index > array.length) {
            resize(index);
        }
        array[index] = item;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int index) {
        Object result = array[index];
        array[index] = null;
        return (T) result;
    }

    private void resize() {
        Object[] newArray = new Object[size() + 1];
        System.arraycopy(array, 0, newArray, 0, size());
        array = newArray;
    }

    private void resize(int index) {
        Object[] newArray = new Object[index + 1];
        System.arraycopy(array, 0, newArray, 0, size());
        array = newArray;
    }
}
